//
// Game
//
class Game {

    constructor() {
        this.startTime = null;
    }

    run() {
        this.start("easy");
        this.registerMouse();
    }

    start(level) {
        this.level = level;
        this.gameEnded = false;
        this.hideMessages();
        this.createBoard();
        this.startTimer();
        this.updateScore();
    }

    restart(level) {
        this.board.destroyHtmlCells();
        this.start(level);
    }

    onMouseDown(e) {
        if (!this.gameEnded) {
            const img = getMouseObject(e);
            if (this.board.isCell(img)) {
                if (getMouseButton(e) == 1) {
                    this.onLeftClick(img.mRow, img.mCol);
                } else {
                    this.onRightClick(img.mRow, img.mCol);
                }
            }
            this.updateScore();
        }
        e.stopPropagation();
    }

    onLeftClick(row, col) {
        if (this.board.isFlag(row, col)) {
            return;
        }
        if (this.board.isMine(row, col)) {
            this.looseGame(row, col);
            return;
        }
        this.board.flip(row, col);
        if (0 == this.board.downs) {
            this.winGame();
        }
    }

    onRightClick(row, col) {
        if (this.board.isDown(row, col)) {
            return;
        }
        this.board.changeState(row, col);
        if (this.board.flags == this.board.mines) {
            this.tryWinGame();
        }
    }

    tryWinGame() {
        if (this.board.testFlags()) {
            this.winGame();
        }
    }

    winGame() {
        this.board.showFlags();
        this.saveTopScore();
        this.showWinMessage();
        this.endGame(true);
    }

    looseGame(row, col) {
        this.board.showMines(row, col);
        this.showLooseMessage();
        this.endGame();
    }

    hideMessages() {
        document.getElementById("message-win").style.display = 'none';
        document.getElementById("message-loose").style.display = 'none';
    }

    showLooseMessage() {
        document.getElementById("message-loose").style.display = 'block';
    }

    showWinMessage() {
        document.getElementById("message-win").style.display = 'block';
    }

    endGame() {
        this.stopTimer();
        this.gameEnded = true;
    }

    saveTopScore() {
        // Check browser support
        if (typeof(Storage) !== "undefined") {
            var bestScoreString = localStorage.getItem(this.level);
            if (typeof(bestScoreString) !== "undefined" && bestScoreString != null && bestScoreString !== "") {
                if (parseInt(bestScoreString.replace(":", "")) > parseInt(this.getTimeString().replace(":", ""))) {
                    localStorage.setItem(this.level, this.getTimeString());
                }
            } else {
                localStorage.setItem(this.level, this.getTimeString());
            }
        }
    }

    updateScore() {
        document.getElementById("div-mines").innerHTML = String(this.board.mines - this.board.flags);
    }

    startTimer() {
        this.startTime = new Date().getTime();
        this.timer();
    }

    stopTimer() {
        this.startTime = null;
    }

    timer() {
        if (this.startTime) {
            document.getElementById("div-time").innerHTML = this.getTimeString();
            setTimeout(this.timer.bind(this), 1000);
        }
    }

    getTimeString() {
        const diff = Math.floor(( new Date().getTime() - this.startTime) / 1000);
        const mins = "0" + String(Math.floor(diff / 60));
        const secs = "0" + String(diff % 60);

        return mins.substring(mins.length - 2) + ":" + secs.substring(secs.length - 2);
    }

    registerMouse() {
        this.board.div.addEventListener("mousedown", this.onMouseDown.bind(this));
        this.board.div.addEventListener('contextmenu', event => event.preventDefault());
    }

    createBoard() {
        switch (this.level) {
            case "easy"    :
                this.board = new Board(10, 10, 10);
                break;
            case "normal"  :
                this.board = new Board(15, 15, 25);
                break;
            case "advanced":
                this.board = new Board(20, 20, 50);
                break;
            case "hard"    :
                this.board = new Board(20, 25, 100);
                break;
            case "expert"  :
                this.board = new Board(20, 30, 150);
                break;
        }
    }
}

//
// Board
//
class Board {

    constructor(rows, cols, mines) {
        this.div = document.querySelector("#div-board");

        this.cellClass = "cell";
        this.cellWidth = 30;
        this.cellHeight = 30;
        this.cellBorderWidth = 1;
        this.imgURL = "Minesweeper v1.0_files/";
        this.imgExt = ".png";
        this.create(rows, cols, mines);
    }

    create(rows, cols, mines) {
        this.cells = null;
        this.rows = rows;
        this.cols = cols;
        this.mines = mines;
        this.downs = (this.rows * this.cols) - this.mines;
        this.flags = 0;

        this.createCells();
        this.putMines();
        this.createHtmlCells();
    }

    createCells() {
        this.cells = new Array(this.rows);
        for (let row = 0; row != this.rows; ++row) {
            this.cells[row] = new Array(this.cols);
            for (let col = 0; col != this.cols; ++col) {
                this.cells[row][col] = new Cell();
            }
        }
    }

    putMines() {
        for (let mine = 0; mine != this.mines; ++mine) {
            this.putRandMine();
        }
    }

    putRandMine() {
        let row, col;
        do {
            row = rand(this.rows);
            col = rand(this.cols);
        } while (this.isMine(row, col));

        this.putMine(row, col);
        this.roundMine(row, col);
    }

    putMine(row, col) {
        this.cells[row][col].value = 'm';
    }

    roundMine(row, col) {
        for (let r = Math.max(row - 1, 0); r <= Math.min(row + 1, this.rows - 1); ++r) {
            for (let c = Math.max(col - 1, 0); c <= Math.min(col + 1, this.cols - 1); ++c) {
                if (this.isMine(r, c) == false) {
                    ++this.cells[r][c].value;
                }
            }
        }
    }

    createHtmlCells() {
        const boardWidth = String(this.cols * (this.cellWidth + this.cellBorderWidth * 2)) + "px";

        document.querySelector("#div-score").style.width = boardWidth;
        this.div.style.width = boardWidth;
        for (let row = 0; row != this.rows; ++row) {
            for (let col = 0; col != this.cols; ++col) {
                this.createHtmlCell(row, col);
            }
        }
    }

    createHtmlCell(row, col) {
        const cellSpan = document.createElement("span");

        cellSpan.id = this.getCellId(row, col);
        cellSpan.className = this.getCellClass(row, col);
        cellSpan.style.width = String(this.cellWidth) + "px";
        cellSpan.style.height = String(this.cellHeight) + "px";
        cellSpan.style.borderWidth = String(this.cellBorderWidth) + "px";
        // img.style.top = String(Math.floor(( (340 - this.cellHeight * this.rows) / 2) + (row * (this.cellHeight - 1) ))) + "px";
        // img.style.left = String(Math.floor(( (510 - this.cellWidth * this.cols) / 2) + (col * (this.cellWidth - 1) ))) + "px";
        cellSpan.mRow = row;
        cellSpan.mCol = col;

        this.div.appendChild(cellSpan);
    }

    destroyHtmlCells() {
        for (let row = 0; row != this.rows; ++row) {
            for (let col = 0; col != this.cols; ++col) {
                this.destroyHtmlCell(row, col);
            }
        }
    }

    destroyHtmlCell(row, col) {
        this.div.removeChild(document.getElementById(this.getCellId(row, col)));
    }

    flip(row, col) {
        if (this.isDown(row, col)) {
            return;
        }
        if (this.isFlag(row, col)) {
            return;
        }
        this.flipCell(row, col);
        if (this.isHole(row, col)) {
            this.roundFlip(row, col);
        }
    }

    flipCell(row, col) {
        this.cells[row][col].state = "down";
        this.getCellElement(row, col).className = this.getCellClass(row, col);
        --this.downs;
    }

    roundFlip(row, col) {
        const left = (col > 0);
        const right = (col < this.cols - 1);
        if (row > 0) {
            if (left) {
                this.flip(row - 1, col - 1);
            }
            this.flip(row - 1, col);
            if (right) {
                this.flip(row - 1, col + 1);
            }
        }
        if (left) {
            this.flip(row, col - 1);
        }
        if (right) {
            this.flip(row, col + 1);
        }
        if (row < this.rows - 1) {
            if (left) {
                this.flip(row + 1, col - 1);
            }
            this.flip(row + 1, col);
            if (right) {
                this.flip(row + 1, col + 1);
            }
        }
    }

    changeState(row, col) {
        const change = this.getNextState(row, col);
        if (("flag" == change) && (this.flags == this.mines)) {
            return;
        }
        if ("flag" == change) {
            ++this.flags;
        }
        if ("question" == change) {
            --this.flags;
        }

        this.cells[row][col].state = change;
        this.getCellElement(row, col).className = this.getCellClass(row, col);
    }

    getNextState(row, col) {
        switch (this.cells[row][col].state) {
            case "up"      :
                return "flag";
            case "flag"    :
                return "question";
            case "question":
                return "up";
        }
    }

    testFlags() {
        for (let row = 0; row != this.rows; ++row) {
            for (let col = 0; col != this.cols; ++col) {
                if (this.isFlag(row, col) && ( this.isMine(row, col) == false )) {
                    return (false);
                }
            }
        }
        return true;
    }

    showMines(rowBoom, colBoom) {
        for (let row = 0; row != this.rows; ++row) {
            for (let col = 0; col != this.cols; ++col) {
                if (this.isMine(row, col) || this.isFlag(row, col)) {
                    this.showMine(row, col, rowBoom, colBoom);
                }
            }
        }
    }

    showMine(row, col, rowBoom, colBoom) {
        let state = "mine";
        if (this.isFlag(row, col)) {
            state = this.isMine(row, col) ? "flag" : "cross";
        }
        if ((row == rowBoom) && (col == colBoom)) {
            state = "boom";
        }
        this.cells[row][col].state = state;
        this.getCellElement(row, col).className = this.getCellClass(row, col);
    }

    showFlags() {
        for (let row = 0; row != this.rows; ++row) {
            for (let col = 0; col != this.cols; ++col) {
                if (this.isMine(row, col) && ( this.isFlag(row, col) == false)) {
                    this.getCellElement(row, col).src = this.getImgSrc("flag");
                    ++this.flags;
                }
            }
        }
    }

    isMine(row, col) {
        return 'm' == this.cells[row][col].value;
    }

    isHole(row, col) {
        return 0 == this.cells[row][col].value;
    }

    isFlag(row, col) {
        return 'flag' == this.cells[row][col].state;
    }

    isDown(row, col) {
        return 'down' == this.cells[row][col].state;
    }

    isCell(img) {
        return img.className.indexOf(this.cellClass) > -1;
    }

    getCellElement(row, col) {
        return document.getElementById(this.getCellId(row, col));
    }

    getCellId(row, col) {
        return this.cellClass + String((row * this.cols) + col);
    }

    getCellClass(row, col) {
        const cell = this.cells[row][col];
        if (cell.state === "down") {
            return this.cellClass + " " + cell.state + " number-" + cell.value;
        } else {
            return this.cellClass + " " + cell.state;
        }
    }

    getImgSrc(pic) {
        return this.imgURL + pic + this.imgExt;
    }
}

//
// Cell
//
class Cell {

    constructor() {
        this.value = 0;
        this.state = 'up';
    }
}

//
// Utils
//
function rand(x) {
    return Math.floor(Math.random() * x)
}

function getMouseObject(e) {
    return e ? e.target : window.event.srcElement;
}
function getMouseButton(e) {
    return e ? e.which : window.event.button;
}

function showScore() {
    document.getElementById('myModal').style.display = "block";
}

function hideScore() {
    document.getElementById('myModal').style.display = "none";
}


// Get the modal
var modal = document.getElementById('scoreModal');

// Get the button that opens the modal
var btn = document.getElementById("showModal");

// Get the <span> element that closes the modal
var span = document.getElementsByClassName("close")[0];

// When the user clicks the button, open the modal
btn.onclick = function (e) {
    if (typeof(Storage) !== "undefined") {
        var levels = ["easy", "normal", "advanced", "hard", "expert"];
        for (let i = 0; i < levels.length; i++) {
            var levelScore = localStorage.getItem(levels[i]);
            // Retrieve
            if (typeof(levelScore) !== "undefined" && levelScore != null && levelScore !== "") {
                document.getElementById("score_" + levels[i]).innerHTML = levelScore;
            } else {
                document.getElementById("score_" + levels[i]).innerHTML = "Not win, yet";
            }
        }

        modal.style.display = "block";
    } else {
        alert("Sorry, your browser does not support Web Storage...");
    }
    e.preventDefault();
};

// When the user clicks on <span> (x), close the modal
span.onclick = function () {
    modal.style.display = "none";
};

// When the user clicks anywhere outside of the modal, close it
window.onclick = function (event) {
    if (event.target == modal) {
        modal.style.display = "none";
    }
};
//
// Instance and start Game
//
var game = new Game();
game.run();
